import java.time.LocalDateTime;       

public class Main{
	public static void main (String[] args){
		
		//Creating an array of book class
		Book[] books = new Book[5];
		 
		LocalDateTime currentDate = LocalDateTime.now();  
		
		//instantiate all the books
		books[0] = new Book(1101, "Rabindranath Tagore", "Gitanjoli", "121212-212-212", currentDate); 
		books[1] = new Book(1102, "Kazi Nozrul Islam", "Dolonchapa", "121212-212-212", currentDate); 
		books[2] = new Book(1103, "johir Raihan", "Arek Flagun", "121212-212-212", currentDate); 
		books[3] = new Book(1104, "Selina Hosen", "Hangor Nodi Granade", "121212-212-212", currentDate); 
		books[4] = new Book(1105, "Humayun Ahmed", "Noboni", "121212-212-212", currentDate); 
		
		for(int i = 0; i < books.length; i++){
			
			books[i].print();
			
		}
		
	}
}