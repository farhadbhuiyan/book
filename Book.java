import java.time.LocalDateTime;   

public class Book {
	
	private int bookId;
	private String bookAuthor;
	private String bookName;
	private String ISBN;
	private LocalDateTime publishedDate;
	
	public Book(int bookId, String bookAuthor, String bookName, String ISBN, LocalDateTime publishedDate){
		
		this.bookId = bookId;
		this.bookAuthor = bookAuthor;
		this.bookName = bookName;
		this.ISBN = ISBN;
		this.publishedDate = publishedDate;
	}
	
	public void print(){
		
		System.out.println("Book Name: " + this.bookName + "\nAuthor Name: " + this.bookAuthor + "\n" + "Published Date: " + this.publishedDate + "\n");
		
	}
} 